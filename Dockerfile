FROM maven:3-jdk-11-slim AS MAVEN_BUILD
COPY ./pom.xml .
RUN mvn verify -B
COPY ./src .
RUN mvn clean package -B

FROM openjdk:11-slim
RUN mkdir "app"
WORKDIR /app
COPY --from=MAVEN_BUILD ./demo-0.0.1-SNAPSHOT.jar ./demo-app.jar
ENTRYPOINT ["java", "-jar", "demo-app.jar"]